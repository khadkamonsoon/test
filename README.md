
# react-native-react-native-asia-pay-library

## Getting started

`$ npm install react-native-react-native-asia-pay-library --save`

### Mostly automatic installation

`$ react-native link react-native-react-native-asia-pay-library`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-react-native-asia-pay-library` and add `RNReactNativeAsiaPayLibrary.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNReactNativeAsiaPayLibrary.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNReactNativeAsiaPayLibraryPackage;` to the imports at the top of the file
  - Add `new RNReactNativeAsiaPayLibraryPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-react-native-asia-pay-library'
  	project(':react-native-react-native-asia-pay-library').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-react-native-asia-pay-library/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-react-native-asia-pay-library')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNReactNativeAsiaPayLibrary.sln` in `node_modules/react-native-react-native-asia-pay-library/windows/RNReactNativeAsiaPayLibrary.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using React.Native.Asia.Pay.Library.RNReactNativeAsiaPayLibrary;` to the usings at the top of the file
  - Add `new RNReactNativeAsiaPayLibraryPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNReactNativeAsiaPayLibrary from 'react-native-react-native-asia-pay-library';

// TODO: What to do with the module?
RNReactNativeAsiaPayLibrary;
```
  