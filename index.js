
import { NativeModules } from 'react-native';

const { RNReactNativeAsiaPayLibrary } = NativeModules;

export default RNReactNativeAsiaPayLibrary;
